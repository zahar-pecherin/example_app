import React from 'react';
import createReactClass from 'create-react-class';

var PostButtons = createReactClass ({
  render() {
    if (this.props.edit) {
      return (
        <div className="row">
          <div className="col-sm-6 btn-col">
            <button onClick={this.props.requestToUpdate.bind(null, this.props.post.id, this.props.post.params)} className="btn btn-post btn-default">Update</button>
          </div>
          <div className="col-sm-6 btn-col">
            <button onClick={this.props.handleToggle} className="btn btn-post btn-danger">Cancel</button>
          </div>
        </div>
      )
    }
    else {
      return (
        <div className="row">
          <div className="col-sm-6 btn-col">
            <button onClick={this.props.handleToggle} className="btn btn-post btn-success">Edit</button>
          </div>
          <div className="col-sm-6 btn-col">
            <button onClick={this.props.requestToDelete.bind(null, this.props.post.id)} className="btn btn-post btn-danger">Delete</button>
          </div>
        </div>
      );
    }
  }
});

export default PostButtons;