import React from 'react';
import createReactClass from 'create-react-class';
import { connect } from 'react-redux';
import store from '../store';

import { toggleRegistrationFlag, setLoginParams } from '../actions/registration-actions';

import Form from '../components/form';
import Input from '../components/input';

var SignIn = createReactClass ({
  handleToggle() {
    return store.dispatch(toggleRegistrationFlag(!this.props.registration))
  },

  handleChange(e) {
    const this_name = e.target.name;
    const this_value = e.target.value;
    const params = { [this_name]: this_value};
    store.dispatch(setLoginParams(params));
  },

  render() {
    if (this.props.registration) {
      // var signIn = <Registration addSession={this.addSession}/>;
      var linkText = "Уже зарегистрировался? Тогда входи!";
      var url = "/users";
      var className = "registration";
      var buttonLabel = 'Зарегистрироваться!'
    } else {
      // var signIn = <Login addSession={this.addSession}/>;
      var linkText = "Перейти к регистрации";
      var url = "/users/sign_in";
      var className = "login";
      var buttonLabel = 'Войти!';
    }

    var signInBlock = <div className={className}>
      <Form handleNew={this.props.addSession}
            formType="user"
            buttonLabel={buttonLabel}>
        <Input placeholder='Введите логин'
               handleChange={this.handleChange}
               value={this.props.params.email}
               name='email'/>
        <Input placeholder='Введите пароль'
               handleChange={this.handleChange}
               value={this.props.params.password}
               name='password'/>
        {this.props.registration && <Input placeholder='Ещё раз пароль'
                                           handleChange={this.handleChange}
                                           value={this.props.params.password_confirmation}
                                           name='confirm_password'/>}
      </Form>
    </div>;

    return (
      <div className="sign_in">
        <div className="col-sm-3">
          {signInBlock}
        </div>
        <div className="col-sm-9"></div>
        <div className="col-sm-12">
          <a onClick={this.handleToggle}> {linkText} </a>
        </div>
      </div>
    )
  }
});

const mapStateToProps = function(store) {
  return {
    registration: store.registrationState.registration,
    params: store.registrationState.params
  };
};

export default connect(mapStateToProps)(SignIn);