import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom';
import createReactClass from 'create-react-class';

import MainLayout from './components/mainLayout';

var Routes = createReactClass ({
  render() {
    return (
      <Router>
        <Route path='/' component={MainLayout}/>
      </Router>
    )
  }
});

export default Routes;