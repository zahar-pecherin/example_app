class Api::PeopleController < Api::BaseController
  skip_before_action :verify_authenticity_token

  def index
    @users = User.all
    @current_user = current_user ? current_user : {}


    render json: {
        users: @users,
        current_user: @current_user
    }
  end
end
