import React from 'react';
import createReactClass from 'create-react-class';
import { connect } from 'react-redux';
import store from '../store';

var Paginator = createReactClass ({
  shouldComponentUpdate: function(nextProps, nextState, nextContext) {
    return nextProps.page != this.props.page || nextProps.total_pages != this.props.total_pages
  },

  handlePagination: function(page) {
    if (page == this.props.page) {
      return false
    }
    this.props.changePage({page: page})
  },

  handleChangePerPage: function(e) {
    if (e.target.value == this.props.per_page) {
      return false
    }
    this.props.changePage({per_page: e.target.value})
  },

    render: function() {
      var pages = parseInt(this.props.total_pages);
      var current_page = parseInt(this.props.page);
      var next_page = this.props.total_pages > this.props.page && current_page + 1;

      var low_limit = current_page - 2;
      var high_limit = current_page + 2;

      var first_available_page = low_limit > 1 ? low_limit : 1;
      var last_available_page = high_limit < pages ? high_limit : pages;

      var prevAvailable = current_page > 1;
      var nextAvailable = next_page && true;

      var page_nums = [];
      for (var i = first_available_page; i <= last_available_page; i++) {
        page_nums.push(
          <li key={i}><span className={current_page == i ? 'current-pager number-pager': 'valid-pager number-pager'}
                            onClick={this.handlePagination.bind(this, i)}>{i}</span></li>
        );
      }

      var pager = <ul className='menu-secondary-item big pagination'>
        {prevAvailable && <li><span className='valid-pager letter-pager' onClick={this.handlePagination.bind(this, 1)}> First </span></li>}
        {prevAvailable && <li><span className='valid-pager letter-pager' onClick={this.handlePagination.bind(this, current_page - 1)}> Prev </span></li>}

        {first_available_page != 1 && <li><span className='disabled-pager'>...</span></li>}

        {page_nums.map(function(object, i){
            return page_nums[i]
        })}

        {last_available_page != pages && <li><span className='disabled-pager'>...</span></li>}

        {nextAvailable && <li><span className='valid-pager letter-pager' onClick={this.handlePagination.bind(this, next_page)}> Next </span></li>}
        {nextAvailable && <li><span className='valid-pager letter-pager' onClick={this.handlePagination.bind(this, pages)}> Last </span></li>}
      </ul>

      return (
        <div className='row paginator'>
          <div className='col-md-12'>
            {pager}
          </div>
          <div className="paginatorSelect">
            Выбери количество постов на странице
            <select className="custom-select mr-sm-2"
                    defaultValue={this.props.per_page || 5}
                    onChange={this.handleChangePerPage}
                    id="inlineFormCustomSelect">
                <option value="1">Один</option>
                <option value="3">Три</option>
                <option value="5">Пять</option>
                <option value="10">Десять</option>
            </select>
          </div>
        </div>
      )
    }

});

const mapStateToProps = function(store) {
  return {
    page: store.postState.page,
    per_page: store.postState.per_page,
    total_pages: store.postState.total_pages
  };
};

export default connect(mapStateToProps)(Paginator);