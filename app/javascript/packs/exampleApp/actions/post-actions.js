import * as types from '../actions/action-types';
import axios from 'axios';
import { getCommentsSuccess, addPostCommentsState } from '../actions/comment-actions';
import { push } from 'react-router-redux'

export function getPostsSuccess(posts, page, per_page, total_pages) {
  return {
    type: types.GET_POSTS_SUCCESS,
    posts,
    page,
    per_page,
    total_pages
  };
}

export function getPostSuccess(post) {
  return {
    type: types.GET_POST_SUCCESS,
    post
  };
}


export function addPostSuccess(post) {
  return {
    type: types.ADD_POST_SUCCESS,
    post
  };
}

export function deletePostSuccess(postId) {
  return {
    type: types.DELETE_POST_SUCCESS,
    postId
  };
}

export function changePostForm(object) {
  return {
    type: types.CHANGE_POST_FORM,
    object
  };
}

export function updatePostSuccess(postID, params) {
  return {
    type: types.UPDATE_POST_SUCCESS,
    postID,
    params
  };
}

export function updatePostFail(postID) {
  return {
    type: types.UPDATE_POST_FAIL,
    postID
  };
}

export function changeEdit(postID) {
  return {
    type: types.CHANGE_EDIT,
    postID,
  };
}

export function changePostSuccess(postID, params) {
  return {
    type: types.CHANGE_POST_SUCCESS,
    postID,
    params
  };
}

export function postsIsLoading(loading) {
  return {
    type: types.LOADING_COMMENTS,
    loading
  };
}

export function routingRedirect(url) {
  return {
    type: types.ROUTING,
    url
  };
}

export function requestToDeletePost(postID) {
  return (dispatch) => {
    dispatch(postsIsLoading(true));

    axios.delete('/api/posts/' + postID)
      .then(function (response) {
        dispatch(postsIsLoading(false));
        dispatch(deletePostSuccess(postID));
        if (window.location.pathname != '/posts') {
          dispatch(push('/posts'));
          window.location.reload();
        }
        return response;
      })
      .catch(function (error) {
        console.log(error);
      });
  };
}

export function requestToUpdatePost(postID, params) {
  return (dispatch) => {
    dispatch(postsIsLoading(true));
    dispatch(changeEdit(postID));

    axios.patch('/api/posts/' + postID, {post: params})
      .then(function (response) {
        dispatch(postsIsLoading(false));
        dispatch(updatePostSuccess(postID, params));
        return response;
      })
      .catch(function (error) {
        dispatch(updatePostFail(postID));
      });
  };
}

export function requestToAddPost(params) {
  return (dispatch) => {
    dispatch(postsIsLoading(true));

    axios.post('/api/posts/', {post: params})
      .then(function (response) {
        dispatch(addPostSuccess(response.data));
        dispatch(addPostCommentsState(response.data.id));
        return response;
      })
      .catch(function (error) {
        console.log(error)
      });
    dispatch(postsIsLoading(false));
  };
}

export function requestToLoadPosts(_this, params) {
  return (dispatch) => {
    dispatch(postsIsLoading(true));

    // var current_filter = params.filter || _this.state.filter;
    var current_per_page = params.per_page || _this.props.per_page;
    var current_page = params.per_page ? 1 : params.page || _this.props.page;
    var current_query = params.query || _this.props.query;

    axios.get('/api/posts', { params: {query: current_query, page: current_page, per_page: current_per_page} })
      .then(function(response){
        dispatch(getPostsSuccess(response.data.posts,
          response.data.page,
          response.data.per_page,
          response.data.total_pages ));
        dispatch(getCommentsSuccess(response.data.posts));
        return response;
      })
      .catch(function(error){
        console.log(error)
      });
    dispatch(postsIsLoading(false));
  };
}

export function requestToLoadPost(_this, postId) {
  return (dispatch) => {
    dispatch(postsIsLoading(true));

    axios.get('/api/posts/' + postId)
      .then(function(response){
        dispatch(postsIsLoading(false));
        dispatch(getPostSuccess(response.data));
        dispatch(getCommentsSuccess([response.data]));
        return response;
      })
      .catch(function(error){
        console.log(error)
      });
  };
}
