import React from 'react';
import createReactClass from 'create-react-class';
import { connect } from 'react-redux';
import store from '../store';
import axios from 'axios';

import { requestToDeleteComment, requestToAddComment } from '../actions/comment-actions';
import { addCommentSuccess, changeCommentForm } from '../actions/comment-actions';

import Comment from '../components/comment';
import Form from '../components/form';
import Input from '../components/input';

var CommentList = createReactClass ({

  isThisPost() {
    var _this = this;
    function thisPost(element, index, array) {
      if (element.post_id === _this.props.post.id){
        return element
      }
    }
    return this.props.comments.find(thisPost);
  },

  addComment() {
    var postID = this.props.post.id;
    var params = this.isThisPost().params;

    return axios.post('/api/posts/' + postID + '/comments', {comment: params})
      .then(function (response) {
        store.dispatch(addCommentSuccess(response.data, postID));
        return response;
      })
      .catch(function (error) {
        // _this.setState({error: "Все поля должны быть заполнены!"})
      });
  },

  handleChange(e) {
    const content = e.target.value;
    var postID = this.props.post.id;
    store.dispatch(changeCommentForm(content, postID));
  },

  render: function() {
    var _this = this;
    var params = '';
    var comments = [];
    var element = this.isThisPost();

    if (element) {
      params = element.params;
      comments = element.comments
     }

    return (
      <div className="comments">
        <div className="row comment-row">
          <div className="col-sm-2">
          </div>
          <div className="col-sm-8">
            {comments.map(comment => {
              return(
                <Comment post={_this.props.post}
                         comment={comment} key={comment.id}
                         handleDelete={this.props.requestToDelete.bind(null, _this.props.post.id, comment.id)}/>
              )}
            )}
          </div>
          <div className="col-sm-2">
          </div>
        </div>

        <div className="row">
          <div className="col-sm-2"></div>
          <div className="col-sm-8">
            <Form handleNew={this.props.requestToAdd.bind(null, _this.props.post.id, params)}
                  formType="comment"
                  buttonLabel='Отправить'>

              <Input placeholder='Оставьте комментарий'
                     name='content'
                     value={params.content}
                     handleChange={this.handleChange}/>
            </Form>
          </div>
        </div>
      </div>
    )
  },
});

const mapStateToProps = function(store) {
  return {
    comments: store.commentState.comments,
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    requestToDelete: function(post_id, comment_id) {dispatch(requestToDeleteComment(post_id, comment_id))},
    requestToAdd: function(post_id, comment_id) {dispatch(requestToAddComment(post_id, comment_id))},
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CommentList);

