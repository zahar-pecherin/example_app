import React from 'react';
import createReactClass from 'create-react-class';

var PostForm = createReactClass ({

  render() {
    return (
      <div className="row">
        <div className="col-sm-4">
          <input type='text'
                 className='form-control'
                 placeholder={this.props.post.title}
                 name='title'
                 defaultValue={this.props.post.title}
                 onChange={this.props.handleChange}/>
        </div>
        <div className="col-sm-8">
          <input type='text'
                 className='form-control'
                 placeholder={this.props.post.body}
                 name='body'
                 defaultValue={this.props.post.body}
                 onChange={this.props.handleChange}/>
        </div>
      </div>
    )
  }
});

export default PostForm;