import * as types from '../actions/action-types';

const initialState = '';

const filterReducer = function(state = initialState, action) {
  switch(action.type) {
    case types.FILTER_POSTS:
      return action.query;
  }
  return state;
};

export default filterReducer;