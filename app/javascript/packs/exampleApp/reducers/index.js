import { combineReducers } from 'redux';

// Reducers
import userReducer from './user-reducer';
import postReducer from './post-reducer';
import commentReducer from './comment-reducer';
import searchReducer from './search-reducer';
import registrationReducer from './registration-reducer';
import filterReducer from './filter-reducer';

var reducers = combineReducers({
  userState: userReducer,
  postState: postReducer,
  commentState: commentReducer,
  searchState: searchReducer,
  registrationState: registrationReducer,
  filterState: filterReducer,
});

export default reducers;