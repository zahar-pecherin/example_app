class Api::CommentsController < Api::BaseController
  before_action :set_post
  before_action :set_comment, only: [:update, :destroy, :user]
  skip_before_action :verify_authenticity_token

  def index
    @comments = @post.comments.map{|comment| comment.as_json.merge(user_email: comment.user.email)}

    render json: @comments
  end

  def create
    @comment = @post.comments.new(comment_params)
    @comment.user_id = current_user.id
    if @comment.save
      # render json: @comment.as_json.merge(user_email: @comment.user.email)
      render json: @comment.as_json.merge(user: @comment.user)
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  def update
    if @comment.update(post_params)
      render json: @comment.as_json.merge(user: @comment.user)
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @comment.destroy
    head :no_content
  end

  def user
    @user = @comment.user.email
    render json: @user
  end

  private

  def set_post
    @post = Post.find(params[:post_id])
  end

  def set_comment
    @comment = @post.comments.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:content)
  end
end