import * as types from '../actions/action-types';
import { postsIsLoading } from '../actions/post-actions';
import axios from 'axios';

export function setAutocompleteQuery(posts, query) {
  return {
    type: types.SET_AUTOCOMPLETE_QUERY,
    posts,
    query
  };
}

export function requestToChangeQuery(e) {
  return (dispatch) => {
    dispatch(postsIsLoading(true));

    var current_query = e.target.value;

    if (current_query != "") {
      axios.get('/api/posts/autocomplete', {params: { query: current_query} })
        .then(function(response){
          dispatch(postsIsLoading(false));
          dispatch(setAutocompleteQuery(response.data.posts, current_query));
          return response;
        })
        .catch(function(error){
          console.log(error)
        });
    } else {
      dispatch(setAutocompleteQuery([], ''));
    }

  };
}