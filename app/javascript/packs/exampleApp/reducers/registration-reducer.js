import * as types from '../actions/action-types';

const initialState = {
  current_user: {},
  params: {email: '', password: '', password_confirmation: ''},
  registration: false
};

const registrationReducer = function(state = initialState, action) {
  switch(action.type) {
    case types.SET_CURRENT_USER:
      return Object.assign({}, state, { current_user: action.current_user });
  }

  switch(action.type) {
    case types.TOGGLE_REGISTRATION_FLAG:
      return Object.assign({}, state, { registration: action.registration });
  }

  switch(action.type) {
    case types.SET_LOGIN_PARAMS:
      const paramsKey = Object.keys(action.params)[0];
      const paramsValue = Object.values(action.params)[0];

      if (paramsKey === 'email') {
        return {...state,
          params: {email: paramsValue, password: state.params.password, password_confirmation: state.params.password_confirmation }
        };
      } else if (paramsKey === 'password') {
        return {...state,
          params: {email: state.params.email, password: paramsValue, password_confirmation: state.params.password_confirmation }
        };
      } else {
        return {...state,
          params: {email: state.params.email, password: state.params.password, password_confirmation: paramsValue }
        };
      }
  }

  switch(action.type) {
    case types.RESET_LOGIN_PARAMS:
      return Object.assign({}, state, {params: {email: '', password: '', password_confirmation: ''}});
  }
  return state;
};

export default registrationReducer;
