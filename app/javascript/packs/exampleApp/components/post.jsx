import React from 'react';
import createReactClass from 'create-react-class';
import { connect } from 'react-redux';
import store from '../store';
import { changeEdit, changePostSuccess, requestToDeletePost, requestToUpdatePost } from '../actions/post-actions';

import CommentList from '../components/commentList';
import PostForm from '../components/postForm';
import PostInfo from '../components/postInfo';
import PostButtons from '../components/postButtons';

var Post = createReactClass ({

  handleToggle(e) {
    e.preventDefault();
    return store.dispatch(changeEdit(this.props.post.id));
  },

  handleChange(e) {
    const this_name = e.target.name;
    const this_value = e.target.value;
    const editParams = { [this_name]: this_value};
    store.dispatch(changePostSuccess(this.props.post.id, editParams));
  },

  postView() {
    if (this.props.post.edit) {
      return (
        <PostForm handleChange={this.handleChange}
                  handleToggle={this.handleToggle}
                  requestToUpdate={this.props.requestToUpdate}
                  current_user={this.props.current_user}
                  post={this.props.post} />
      )
    } else {
      return (
        <PostInfo requestToDelete={this.props.requestToDelete}
                  handleToggle={this.handleToggle}
                  current_user={this.props.current_user}
                  post={this.props.post} />
      )
    }
  },

  render() {
    return (
      <div className="post">
        <div className="row">
          <div className="col-sm-8 col-lg-9">
            {this.postView()}
          </div>
          <div className="col-sm-4 col-lg-3">
            <PostButtons post={this.props.post}
                         edit={this.props.post.edit}
                         handleToggle={this.handleToggle}
                         requestToUpdate={this.props.requestToUpdate}
                         requestToDelete={this.props.requestToDelete} />
          </div>
        </div>

        <CommentList post={this.props.post}
                     key={this.props.post.id}
        />
      </div>
    );
  }
});

const mapStateToProps = function(store) {
  return {
    posts: store.postState.posts,
    current_user: store.registrationState.current_user,
    users: store.userState.users
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    requestToDelete: function(post_id) {dispatch(requestToDeletePost(post_id))},
    requestToUpdate: function(post_id, params) {dispatch(requestToUpdatePost(post_id, params))}
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Post);