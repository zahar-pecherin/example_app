import React from 'react'
import createReactClass from 'create-react-class';

var HomePage = createReactClass({
  render() {
    return(
      <div className="home-page">
        Hello world
      </div>
    )
  }
});

export default HomePage;