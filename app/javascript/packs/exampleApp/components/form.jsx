import React from 'react';
import createReactClass from 'create-react-class';
import PropTypes from 'prop-types';

var Form = createReactClass({

  render() {
    return (
      <div className='form'>
        {this.props.children}
        <button onClick={this.props.handleNew}
                className='btn btn-primary'>
          {this.props.buttonLabel}
        </button>
      </div>
    )
  }

});

Form.propTypes = {
  handleNew: PropTypes.func,
  buttonLabel: PropTypes.string,
};

export default Form;