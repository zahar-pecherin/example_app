import * as types from '../actions/action-types';

const initialState = {
  searchPosts: [],
  query: ''
};

const searchReducer = function(state = initialState, action) {
  switch(action.type) {
    case types.SET_AUTOCOMPLETE_QUERY:
      return Object.assign({}, state, { searchPosts: action.posts , query: action.query});
  }
  return state;
};

export default searchReducer;