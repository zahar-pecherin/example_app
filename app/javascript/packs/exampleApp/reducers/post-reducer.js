import * as types from '../actions/action-types';
import _ from 'lodash';

const initialState = {
  posts: [],
  post: {},
  object: {title: '', body: ''},
  page: 1,
  per_page: 5,
  total_pages: 0,
  loading: false
};

const postReducer = function(state = initialState, action) {

  switch(action.type) {
    case types.LOADING_POSTS:
      return Object.assign({}, state, { loading: action.loading });

    case types.GET_POSTS_SUCCESS:
      var actionPosts = action.posts.map(post => Object.assign({}, post, {edit: false, params: {title: post.title, body: post.body}}));
      return Object.assign({}, state, { posts: actionPosts, page: action.page, per_page: action.per_page, total_pages: action.total_pages});

    case types.GET_POST_SUCCESS:
      return Object.assign({}, state, { post: action.post });

    case types.ADD_POST_SUCCESS:
      const withinPosts = _.union([action.post], state.posts);
      return Object.assign({}, state, { posts: withinPosts, object: {title: '', body: ''} });

    case types.DELETE_POST_SUCCESS:
        // Use lodash to create a new post array without the post we want to remove
      const newPosts = _.filter(state.posts, post => post.id != action.postId);
      return Object.assign({}, state, { posts: newPosts, post: {} });

    case types.CHANGE_POST_SUCCESS:
      const paramsKey = Object.keys(action.params)[0];
      const paramsValue = Object.values(action.params)[0];

      if (paramsKey === 'title'){
        return {...state,
          posts: state.posts.map(post => post.id === action.postID ?
            { ...post, title: post.title, body: post.body, edit: post.edit, params: {body: post.params.body, title: paramsValue} } : post
          ),
          post: Object.assign({},  state.post, { edit: state.post.edit, params: {title: paramsValue, body: state.post.body} })
        }
      } else {
        return {...state,
          posts: state.posts.map(post => post.id === action.postID ?
            { ...post, title: post.title, body: post.body, edit: post.edit, params: {body: paramsValue, title: post.params.title} } : post
          ),
          post: Object.assign({},  state.post, { edit: state.post.edit, params: {title: state.post.title, body: paramsValue} })
        }
      }

    case types.UPDATE_POST_SUCCESS:
      return {...state,
        posts: state.posts.map(post => post.id === action.postID ?
          { ...post, title: action.params.title, body: action.params.body } : post
        ),
        post: Object.assign({},  state.post, { title: action.params.title, body: action.params.body })
      };

    case types.UPDATE_POST_FAIL:
      return {...state,
        posts: state.posts.map(post => post.id === action.postID ?
          { ...post, title: post.title, body: post.body, params: {title: post.title, body: post.body} } : post
        )
      };

    case types.CHANGE_EDIT:
      return {...state,
        posts: state.posts.map(post => post.id === action.postID ?
          { ...post, title: post.title, body: post.body, edit: !post.edit, params: {title: post.title, body: post.body} } : post
        ),
        post: Object.assign({},  state.post, { edit: !state.post.edit, params: {title: state.post.title, body: state.post.body} })
      };

    case types.CHANGE_POST_FORM:
      const objectKey = Object.keys(action.object)[0];
      const objectvalue = Object.values(action.object)[0];

      if (objectKey === 'title'){
        return {...state,
          object: { title: objectvalue, body: state.object.body }
        }
      } else {
        return {...state,
          object: {body: objectvalue, title: state.object.title}
        }
      }
  }
  return state;
};

export default postReducer;