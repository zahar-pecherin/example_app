class Api::PostsController < Api::BaseController
  before_action :set_post, only: [:show, :update, :destroy]
  skip_before_action :verify_authenticity_token

  def index
    if params[:query] == ''
      @posts = Post.order(id: :desc).eager_load(:user, comments: :user).page(params[:page]).per(params[:per_page] || 5)
    else
      @posts = Post.order(id: :desc).eager_load(:user, comments: :user).search(params[:query]).page(params[:page]).per(params[:per_page] || 5)
    end
    @total_pages = @posts.total_pages
    render json: {
        posts: @posts.map{ |post| post.as_json.merge(comments: post.comments.map{ |comment| comment.as_json.merge(user: comment.user)},
                                                     user: post.user) },
        page: params[:page] || 1,
        per_page: params[:per_page] || 5,
        total_pages: @total_pages
    }
  end

  def show
    render json: @post.as_json.merge(user: @post.user, comments: @post.comments.map{ |comment| comment.as_json.merge(user: comment.user) })
  end


  def create
    @post = current_user.posts.new(post_params)

    if @post.save
      render json: @post.as_json.merge(user: @post.user)
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  def update
    if @post.update(post_params)
      render json: @post.as_json.merge(user: @post.user)
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @post.destroy
    head :no_content
  end

  def autocomplete
    @posts = Post.order(id: :desc).eager_load(:comments).search(params[:query]).page(1).per(5)
    @total_pages = @posts.total_pages
    render json: {
      # posts: @posts.map { |post| post.as_json.merge(comments: post.comments.map { |comment| comment.as_json.merge(user_email: comment.user.email)})},
      posts: @posts.map{ |post| post.as_json.merge(comments: post.comments)},
      query: params[:query] || ''
    }
  end

  private

  def set_post
    @post = Post.eager_load(:user, comments: :user).find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :body)
  end
end