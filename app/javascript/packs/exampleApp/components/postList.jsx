import React from 'react';
import createReactClass from 'create-react-class';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import store from '../store';

import { changePostForm, requestToLoadPosts, requestToAddPost} from '../actions/post-actions';
import { filterPosts } from '../actions/filter-actions';

import Post from '../components/post';
import Form from '../components/form';
import Input from '../components/input';
import Search from '../components/search';
import Paginator from '../components/paginator';

var PostList = createReactClass ({
  getInitialState() {
      return {
          error: '',
      };
  },

  componentDidMount() {
      this.props.loadPosts(this, {});
  },

  loadPosts: function (params = {}) {
      this.props.loadPosts(this, params);
  },

  handleChange(e) {
      const this_name = e.target.name;
      const this_value = e.target.value;
      const object = { [this_name]: this_value};

      store.dispatch(changePostForm(object));
  },

  searchPostWithFilter() {
    const query = this.filterPost.value;
    store.dispatch(filterPosts(query))
  },

  render() {
    let posts = this.props.posts;
    if (this.props.filter != '') {
      posts = posts.filter(post => post.title.includes(this.props.filter));
    }

    return(
      <div className="posts">
        <div className='error-message'>{this.state.error}</div>
        {/*<div className='loading-message' style={this.props.loading ? {display: 'block'} : {display: 'none'}}>*/}
            {/*<div className='loading-message-text'> Loading... </div>*/}
        {/*</div>*/}

        <div className="post-form">
          <Form handleNew={this.props.requestToAdd.bind(null, this.props.object)}
                formType="post"
                buttonLabel='Создать пост'>
            <Input placeholder='Введите название'
                   name='title'
                   value={this.props.object.title}
                   handleChange={this.handleChange}/>
            <Input placeholder='Напишите свои мысли'
                   name='body'
                   value={this.props.object.body}
                   handleChange={this.handleChange}/>
          </Form>
        </div>

        <Search handleSearch={this.loadPosts}
                loadPosts={this.loadPosts}/>
        <div>
          Поиск с помощью фильтра на Redux
          <div className='row'>
            <div className="col-sm-2"></div>
            <div className="col-sm-8 ">
              <div className='form-group search-form-group'>
                <input type='text' ref={(input) => { this.filterPost = input }}/>
              </div>
            </div>
            <div className="col-sm-2">
              <button className='btn  btn-primary'
                      onClick={this.searchPostWithFilter.bind(null, this)}>
                Поиск
              </button>
            </div>
          </div>
        </div>
        <div className="data-list">
          {posts.map((post) => {
            return (
              <div key={post.id} className="data-list-item">
                <Post post={post}
                      key={post.id}
                      handleChange={this.handleChange}
                      />
              </div>
            );
          })}
        </div>
        <Paginator changePage={this.loadPosts}
                   ChangePerPage={this.loadPosts}/>
      </div>
    )
  },
});

const mapStateToProps = function(store) {
  return {
    posts: store.postState.posts,
    object: store.postState.object,
    query: store.searchState.query,
    page: store.postState.page,
    per_page: store.postState.per_page,
    total_pages: store.postState.total_pages,
    current_user: store.registrationState.current_user,
    loading: store.postState.loading,
    filter: store.filterState
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    requestToAdd: function(params) {dispatch(requestToAddPost(params))},
    loadPosts: function(_this, params) {dispatch(requestToLoadPosts(_this, params))},
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostList);