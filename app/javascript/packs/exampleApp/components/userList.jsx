import React from 'react'
import createReactClass from 'create-react-class';

var UserList = createReactClass ({
  getInitialState: function() {
    return {
      users: []
    }
  },

  render() {
    return(
      <div className="search">
        <header className="search-header"></header>
        <div className="results">
          <ul className="user-list">
            <li>Dan</li>
            <li>Ryan</li>
            <li>Michael</li>
          </ul>
        </div>
        <div className="search-footer pagination"></div>
      </div>
    )
  }
});

export default UserList;