import React from 'react';
import createReactClass from 'create-react-class';
import { connect } from 'react-redux';

var Comment = createReactClass ({
  deleteButton() {
    if (this.props.post.user_id === this.props.current_user.id || this.props.comment.user_id === this.props.current_user.id)   {
      return(
        <div className="col-sm-2 btn-col col-comment-delete">
          <div className="right-alignment width-45 ">
            <a className='btn-comment' onClick={this.props.handleDelete}> X </a>
          </div>
        </div>
      )
    } else {
      return (<div className="col-sm-2"></div>)
    }
  },

  commentRow() {
    var _this = this;
    return (
      <div className="row">
        <div className="col-sm-2 col-comment-user">
          {this.props.comment.user.email}:
        </div>
        <div className="col-sm-8 col-comment-content">
          {this.props.comment.content}
        </div>
        {this.deleteButton()}
      </div>
    )
  },

  render() {
    return this.commentRow();
  }
});

const mapStateToProps = function(store) {
  return {
    current_user: store.registrationState.current_user,
    users: store.userState.users
  };
};

export default connect(mapStateToProps)(Comment);