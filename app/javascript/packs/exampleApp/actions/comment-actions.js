import * as types from '../actions/action-types';
import axios from 'axios';

export function getCommentsSuccess(posts) {
  return {
    type: types.GET_COMMENTS_SUCCESS,
    posts
  };
}

export function addCommentSuccess(comment, postID) {
  return {
    type: types.ADD_COMMENT_SUCCESS,
    comment,
    postID
  };
}

export function deleteCommentSuccess(postID, commentID) {
  return {
    type: types.DELETE_COMMENT_SUCCESS,
    postID,
    commentID
  };
}

export function changeCommentForm(content, postID) {
  return {
    type: types.CHANGE_COMMENT_FORM,
    content,
    postID
  };
}

export function addPostCommentsState(postID) {
  return {
    type: types.ADD_POST_COMMENTS_STATE,
    postID
  };
}

export function commentIsLoading(loading) {
  return {
    type: types.LOADING_COMMENTS,
    loading
  };
}

export function requestToDeleteComment(postID, commentID) {
  return (dispatch) => {
    dispatch(commentIsLoading(true));

    axios.delete('/api/posts/' + postID + '/comments/' + commentID)
      .then(function (response) {
        dispatch(deleteCommentSuccess(postID, commentID));
        return response;
      })
      .catch(function (error) {
        console.log(error);
      });
    dispatch(commentIsLoading(false));
  };
}

export function requestToAddComment(postID, params) {
  return (dispatch) => {
    dispatch(commentIsLoading(true));

    axios.post('/api/posts/' + postID + '/comments', {comment: params})
      .then(function (response) {
        dispatch(addCommentSuccess(response.data, postID));
        return response;
      })
      .catch(function (error) {
        console.log(error);
      });
    dispatch(commentIsLoading(false));
  };
}