import * as types from '../actions/action-types';
import _ from 'lodash';

const initialState = {
  comments: [],
  loading: false
};

const commentReducer = function(state = initialState, action) {

  switch(action.type) {
    case types.LOADING_COMMENTS:
      return Object.assign({}, state, { loading: action.loading });

    case types.GET_COMMENTS_SUCCESS:
      var actionComments = action.posts.map(post => Object.assign({}, {post_id: post.id}, {comments: post.comments, params: {content: ''}}));
      return Object.assign({}, state, { comments: actionComments });

    case types.ADD_POST_COMMENTS_STATE:
      return {...state,
        comments: state.comments.concat([{post_id: action.postID, comments: [], params: {content: ''}}])
      };

    case types.ADD_COMMENT_SUCCESS:
      return {...state,
        comments: state.comments.map(commentList => commentList.post_id === action.postID ?
          { ...commentList, post_id: action.postID, comments: commentList.comments.concat([action.comment]), params: { content: '' } } : commentList
        )
      };

    case types.DELETE_COMMENT_SUCCESS:
      return {...state,
        comments: state.comments.map(commentList => commentList.post_id === action.postID ?
          { ...commentList, post_id: action.postID, comments: commentList.comments.filter( comment => comment.id != action.commentID) } : commentList
        )
      };

    case types.CHANGE_COMMENT_FORM:
      return {...state,
        comments: state.comments.map(commentList => commentList.post_id === action.postID ?
          { ...commentList, post_id: action.postID, comments: commentList.comments, params: { content: action.content } } : commentList
        )
      };
  }
  return state;
};

export default commentReducer;