import React from 'react';
import createReactClass from 'create-react-class';
import { Link } from 'react-router-dom';


var PostInfo = createReactClass ({
  render() {
    return (
      <div className="row">
        <div className="col-sm-4 col-lg-4">
          <h3> <Link to={ "posts/" + this.props.post.id }>{this.props.post.title}</Link></h3>
          <p> от {this.props.post.user.email} </p>
        </div>
        <div className="col-sm-8 col-lg-8">
          {this.props.post.body}
        </div>
      </div>
    )
  }
});

export default PostInfo;