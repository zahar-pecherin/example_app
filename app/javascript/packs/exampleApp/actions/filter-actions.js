import * as types from '../actions/action-types';

export function filterPosts(query) {
  return {
    type: types.FILTER_POSTS,
    query
  };
}