import React from 'react';
import createReactClass from 'create-react-class';
import { connect } from 'react-redux';
import store from '../store';

import { setAutocompleteQuery, requestToChangeQuery } from '../actions/search-actions';
import { requestToLoadPosts } from '../actions/post-actions';

var Search = createReactClass ({

  handleSearch(e) {
    e.preventDefault();

    if (this.props.query == "") {
      return this.props.handleSearch({query: this.props.query})
      // return this.props.startSearch(this, {query: this.props.query})
    } else if (e.target.value == this.props.query) {
      return false
    }

    store.dispatch(setAutocompleteQuery([], this.props.query));
    return this.props.handleSearch({query: this.props.query, page: 1 });
    // return this.props.startSearch(this, {query: this.props.query, page: 1 })
  },

  handleLeaveSearch (e) {
    let query = this.props.query;
    return store.dispatch(setAutocompleteQuery([], query));
  },

  handleAutocomplete(e) {
    let query = e.target.getAttribute('data');
    store.dispatch(setAutocompleteQuery([], query));

    return this.props.startSearch(this, {query: query, page: 1});
  },

  render() {
    let posts = this.props.searchPosts.map( (post, index) => {
      let title = post.title;
      return <div className='post-search-autocomlete' data={title} key={post.id} onClick={this.handleAutocomplete}>{title}</div>
    });
    let _this = this
    return(
      <form className='search' >
        <div className='row'>
          <div className="col-sm-2"></div>
          <div className="col-sm-8 ">
            <div className='form-group search-form-group'>
              <input type='text'
                     className='form-control '
                     placeholder='Что будем искать?'
                     name='query'
                     value={this.props.query}
                     onChange={function(e) {_this.props.changeAutocompleteQuery(e)} }
                     onClick={function(e) {_this.props.changeAutocompleteQuery(e)} }/>
            </div>
          </div>
          <div className="col-sm-2">
            <button type='submit'
                    className='btn  btn-primary'
                    onClick={this.handleSearch}>
              Поиск
            </button>
          </div>
        </div>
          <div className='row '>
            <div className="col-sm-2"></div>
            <div className="col-sm-8 ">
              <div id='autocomplete-block' className='col-sm-12 autocomplete-block'>
                  {posts}
              </div>
            </div>
          </div>
      </form>
    )
  }
});

const mapStateToProps = function(store) {
  return {
    searchPosts: store.searchState.searchPosts,
    query: store.searchState.query
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    changeAutocompleteQuery: function(e) {dispatch(requestToChangeQuery(e))},
    startSearch: function(_this, params) {dispatch(requestToLoadPosts(_this, params))}
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);