import React from 'react';
import createReactClass from 'create-react-class';
import PropTypes from 'prop-types';

var Input = createReactClass({

  render: function() {
    return(
      <div className='form-group'>
        <textarea type='text'
                  className='form-control'
                  placeholder={this.props.placeholder}
                  name={this.props.name}
                  value={this.props.value}
                  onChange={this.props.handleChange} />
      </div>
    )
  }
});

Input.propTypes = {
  name: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  handleChange: PropTypes.func,
};

export default Input;
