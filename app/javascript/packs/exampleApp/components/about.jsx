import React from 'react'
import createReactClass from 'create-react-class';

var About = createReactClass({
  render() {
    return(
      <div className="about-page">
        This is some information about project
      </div>
    )
  }
});

export default About;