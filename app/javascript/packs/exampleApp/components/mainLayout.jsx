import React from 'react'
import createReactClass from 'create-react-class';
import { Route, Switch, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import store from '../store';
import axios from 'axios';

import { setCurrentUser, resetLoginParams } from '../actions/registration-actions';
import { getUsersSuccess } from '../actions/user-actions';


import SignIn from '../components/signIn';
import About from '../components/about';
import Home from '../components/homePage';
import UserList from '../components/userList';
import PostList from '../components/postList';
import PostView from '../components/postView';

var MainLayout = createReactClass ({

  componentDidMount() {
    return axios.get('/api/people')
      .then(function(response){
        store.dispatch(setCurrentUser(response.data.current_user));
        return response;
      })
      .catch(function(error){
        console.log(error)
    });
  },

  LogOut() {
    var _this = this;

    axios.delete('/users/sign_out')
      .then(function (response) {
        return response;
      });

    store.dispatch(setCurrentUser({}));
    return store.dispatch(resetLoginParams())
  },

  LogIn() {
    var _this = this;
    var params = this.props.params;

    if (this.props.registration) {
      var url  = '/users';
    } else {
      var url = '/users/sign_in';
      delete params['password_confirmation']
    }

    return axios.post(url, {user: params})
      .then(function (response) {
        return store.dispatch(setCurrentUser(response.data));
      });
  },

  displayLoginPage() {
    return (
      <SignIn addSession={this.LogIn}/>
    )
  },

  displayHomePage() {
    return(
      <div className="app">
        <header className="primary-header"></header>
        <a onClick={this.LogOut}>
          Выйти
        </a>

        <div className="container">
          <div className="row">
            <h1>Здесь что-то будет</h1>

          </div>
          <div className="row">
            <div className="col-sm-1">
              <ul className="list-unstyled components">
                <li><Link to="/">Home</Link></li>
                <li><Link to="/users">Users</Link></li>
                <li><Link to="/posts">Posts</Link></li>
                <li><Link to="/about">About</Link></li>
              </ul>
            </div>
            <div className="col-sm-11">
              <div id="content">
                <Switch>
                  <Route exact path='/' component={Home}/>
                  <Route exact path='/users' component={UserList}/>
                  <Route exact path='/posts' component={PostList}/>
                  <Route exact path='/about' component={About}/>
                  <Route exact path='/posts/:userId' component={PostView}/>
                </Switch>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  },

    render: function () {
      if (this.props.current_user.id) {
        return this.displayHomePage()
      } else {
        return this.displayLoginPage()
      }
    }
});

const mapStateToProps = function(store) {
  return {
    current_user: store.registrationState.current_user,
    params: store.registrationState.params,
    registration: store.registrationState.registration
  };
};

export default connect(mapStateToProps)(MainLayout);