import * as types from '../actions/action-types';

export function setCurrentUser(current_user) {
  return {
    type: types.SET_CURRENT_USER,
    current_user
  };
}

export function toggleRegistrationFlag(registration) {
  return {
    type: types.TOGGLE_REGISTRATION_FLAG,
    registration
  };
}

export function setLoginParams(params) {
  return {
    type: types.SET_LOGIN_PARAMS,
    params
  };
}

export function resetLoginParams() {
  return {
    type: types.RESET_LOGIN_PARAMS,
  };
}