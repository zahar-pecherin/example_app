Rails.application.routes.draw do
  devise_for :users , controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }
  root to: "pages#root"

  get '/users', to: "pages#root"
  get '/posts', to: "pages#root"
  get '/widgets', to: "pages#root"
  get '/posts/:id', to: "pages#root"

  # API
  namespace :api do
    resources :people, only: [:index]
    resources :posts do
      resources :comments
      collection do
        get 'autocomplete'
      end
    end
  end
end
