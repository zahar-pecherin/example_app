class Post < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy

  include PgSearch

  pg_search_scope :pg_search, against: %i[title body], using: { tsearch: { prefix: true } }

  scope :search, (->(query) { pg_search(query) if query.present? })

  validates :title, :body, presence: true
end