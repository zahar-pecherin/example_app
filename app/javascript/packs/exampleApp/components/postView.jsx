import React from 'react';
import createReactClass from 'create-react-class';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import store from '../store';
import { requestToLoadPost } from '../actions/post-actions';
import Post from '../components/post';

var PostView = createReactClass ({
  componentDidMount() {
    this.props.loadPost(this, this.props.match.params.userId);
  },

  handleChange(e) {
    const this_name = e.target.name;
    const this_value = e.target.value;
    const object = { [this_name]: this_value};

    store.dispatch(changePostForm(object));
  },

  render() {
    if (this.props.post.id ) {
      return(
        <div>
          <Post post={this.props.post}
                key={this.props.post.id}
          />
        </div>
      );
    } else {
      return(
        <div>Post not found</div>
      );
    }
  }
});

const mapStateToProps = function(store) {
  return {
    post: store.postState.post
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    loadPost: function(_this, postId) {dispatch(requestToLoadPost(_this, postId))},
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostView);
